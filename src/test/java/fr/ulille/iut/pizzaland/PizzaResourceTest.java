package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());

	private PizzaDao pizzaDao;
	private IngredientDao ingredientDao;

	@Override
	protected Application configure() {
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		ingredientDao = BDDFactory.buildDao(IngredientDao.class);
		ingredientDao.createTable();
		pizzaDao = BDDFactory.buildDao(PizzaDao.class);
		pizzaDao.createTableAndIngredientAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		pizzaDao.dropTableAndIngredientAssociation();
		ingredientDao.dropTable();
	}
	
	@Test
	public void testGetEmptyList() {
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/pizzas").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		assertEquals(0, pizzas.size());
	}
	
}
