package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {

	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizza() {
		
	}
	
	public Pizza(String name) {
		this.name = name;
	}

	public Pizza(UUID id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Pizza(UUID id, String name, List<Ingredient> ingredients) {
		this.id = id;
		this.name = name;
		this.ingredients = ingredients;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());

		return dto;
	}
	
}
