package fr.ulille.iut.pizzaland.resources;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());

	private PizzaDao pizzas;

	@Context
	public UriInfo uriInfo;

	public PizzaResource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTableAndIngredientAssociation();
	}

	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzaResource:getAll");

		List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}
	
}
