package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientsAssociation (pizza VARCHAR(128), ingredient VARCHAR(128),"
			+ "FOREIGN KEY (pizza) REFERENCES pizzas(id)," + "FOREIGN KEY (ingredient) REFERENCES ingredients(id),"
			+ "PRIMARY KEY (pizza, ingredient)")
	void createAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createPizzaTable();
		createAssociationTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
	void dropPizzaTable();

	@SqlUpdate("DROP TABLE IF EXISTS pizzaIngredientsAssociation")
	void dropAssociationTable();

	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropAssociationTable();
		dropPizzaTable();
	}
	
	@SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
	void insertPizza(@BindBean Pizza pizza);
	
	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void removePizza(@Bind("id") UUID id);
	
	@SqlUpdate("INSERT INTO pizzaIngredientsAssociation (pizza, ingredient) VALUES (:pizza, :ingredient)")
	void addIngredient(@Bind("pizza") UUID pizza, @Bind("ingredient") UUID ingredient);
	
	@SqlUpdate("DELETE FROM pizzaIngredientsAssociation WHERE pizza = :pizza AND ingredient = :ingredient")
	void removeIngredient(@Bind("pizza") UUID pizza, @Bind("ingredient") UUID ingredient);
	
	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name") String name);
	
	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();
	
	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Ingredient.class)
	Pizza findById(@Bind("id") UUID id);

}
